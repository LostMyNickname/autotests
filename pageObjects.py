# -*- coding: utf-8 -*-


class BasePage(object):
    url = None

    def input_by_id(self, id, value):
        elem = self.driver.find_element_by_id(id)
        elem.send_keys(value)

    def input_by_xpath(self, xpath, value):
        elem = self.driver.find_element_by_xpath(xpath)
        elem.send_keys(value)

    def navigate(self):
        self.driver.get(self.url)

    def is_exact_page(self):
        if self.driver.current_url == self.url:
            print('-'*80)
            print(self.__class__.__name__, ':', self.driver.current_url)
            return True
        else:
            print('*'*36, 'FAILED' ,'*'*36)
            print(self.__class__.__name__, ':', self.driver.current_url)
            return False

    def __init__(self, driver):
        self.driver = driver
        self.driver.implicitly_wait(10)


class HomePage(BasePage):
    url = 'http://ya.ru/'

    def push_button_find(self):
        self.driver.find_element_by_xpath("//input[@type='submit']").submit()


class HabrPage(BasePage):
    url = 'http://habrahabr.ru/post/152653/'

# -*- coding: utf-8 -*-

from selenium import webdriver
# from selenium.common.exceptions import *
import unittest
import pickle
# import os


class BaseUnitTest(unittest.TestCase):

    browser = None

    @classmethod
    def setUpClass(cls):
        fp = None
        # fp = webdriver.FirefoxProfile(os.path.expanduser(
        #   'c:/Users/asm/AppData/Roaming/Mozilla/Firefox/Profiles/...insert_here.../'
        # ))
        # fp.native_events_enabled = False

        cls.browser = webdriver.Firefox(firefox_profile=fp)
        # cls.browser.maximize_window()

    @classmethod
    def tearDownClass(cls):
        pass
        # cls.browser.close()
        # if cls.browser:
        #     cls.browser.quit()

    @classmethod
    def save_cookies(cls):
        pickle.dump(cls.browser.get_cookies(), open("cookies.pkl", "wb"))
        # print('cookies: {}'.format(cls.browser.get_cookies()))

    @classmethod
    def load_cookies(cls):
        cookies = pickle.load(open("cookies.pkl", "rb"))
        for cookie in cookies:
            cls.browser.add_cookie(cookie)

    @classmethod
    def delete_cookies(cls):
        cls.browser.delete_all_cookies()
# -*- coding: utf-8 -*-

from pageObjects import *
from baseUnitTest import *


class OpenHomePage(BaseUnitTest):

    def test_001(self):
        home_page = HomePage(self.browser)
        home_page.navigate()
        self.assertTrue(home_page.is_exact_page())
        home_page.input_by_id('text', 'selenium')
        home_page.push_button_find()
        results_page = home_page
        results_page.driver.find_elements_by_xpath("//div[@class='serp-list']//h2/a")[3].click()
        habr_page = HabrPage(self.browser)
        habr_page.is_exact_page()
        self.save_cookies()


if __name__ == '__main__':
    unittest.main()